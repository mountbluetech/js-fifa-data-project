function getMatchesWonPerTeam(matches) {
  let matchesWonPerTeam = matches.reduce((accu, match) => {
    let homeTeamGoals = match["Home Team Goals"];
    let awayTeamGoals = match["Away Team Goals"];
    let homeTeamName = match["Home Team Name"];
    let awayTeamName = match["Away Team Name"];
    let teamWon =
      homeTeamGoals > awayTeamGoals
        ? homeTeamName
        : homeTeamGoals < awayTeamGoals
        ? awayTeamName
        : null;
    if (teamWon != null) {
      accu[teamWon] ? accu[teamWon]++ : (accu[teamWon] = 1);
    }
    return accu;
  }, {});

  // console.log(matchesWonPerTeam);
  return matchesWonPerTeam;
}

module.exports = getMatchesWonPerTeam;
