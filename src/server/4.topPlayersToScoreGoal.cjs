function getTopProbabiltyPlayersToScoreGoal(players, playersCount = 10) {
  let playerGoals = players.reduce((accu, player) => {
    const regex = /(?<!O)G/g;
    const regexMatches = player["Event"].match(regex);
    const count = regexMatches ? regexMatches.length : 0;
    if (count != 0) {
      if (accu[player["Player Name"]]) {
        accu[player["Player Name"]] += count;
      } else {
        accu[player["Player Name"]] = count;
      }
    }
    return accu;
  }, {});
  let playerMatches = players.reduce((accu, player) => {
    if (accu[player["Player Name"]]) {
      accu[player["Player Name"]]++;
    } else {
      accu[player["Player Name"]] = 1;
    }
    return accu;
  }, {});

  let playerProbabilities = Object.keys(playerGoals).reduce(
    (accu, playerName) => {
      accu[playerName] = playerGoals[playerName] / playerMatches[playerName];
      return accu;
    },
    {}
  );

  let sortedArray = Object.entries(playerProbabilities)
    .sort((a, b) => b[1] - a[1])
    .slice(0, playersCount);

  let sortedObj = Object.fromEntries(sortedArray);
  return sortedObj;
}

module.exports = getTopProbabiltyPlayersToScoreGoal;
