function getRedcardsPerTeamInYear(matches, players, year) {
  let matchIDsInYear = new Set();
  matches
    .filter((match) => match["Year"] == year)
    .reduce((accu, match) => {
      accu.add(match.MatchID);
      return accu;
    }, matchIDsInYear);

  let redcardsPerTeamInYear = players.reduce((accu, player) => {
    if (
      player["Event"].includes("R") &&
      matchIDsInYear.has(player["MatchID"])
    ) {
      let teamInitials = player["Team Initials"];
      if (accu[teamInitials]) {
        accu[player["Team Initials"]]++;
      } else {
        accu[player["Team Initials"]] = 1;
      }
    }
    return accu;
  }, {});
  return redcardsPerTeamInYear;
}

module.exports = getRedcardsPerTeamInYear;
