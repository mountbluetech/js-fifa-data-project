let path = require("path");
let matchesPath = path.join(__dirname, "../data/WorldCupMatches.csv");
let playersPath = path.join(__dirname, "../data/WorldCupPlayers.csv");
let csv = require("csvtojson");
let fs = require("fs");
let matchesPlayedPerCity = require("./1.matchesPlayedPerCity.cjs");
let matchesWonPerTeam = require("./2.matchesWonPerTeam.cjs");
let getRedcardsPerTeamInYear = require("./3.redcardsPerTeamInYear.cjs");
let getTopProbabiltyPlayersToScoreGoal = require("./4.topPlayersToScoreGoal.cjs");

csv()
  .fromFile(matchesPath)
  .then((matches) => {
    //1.invokes matchesPlayedPerCity function which takes matches file as argument and returns the output object.
    //writes it to the json file
    fs.writeFile(
      path.join(__dirname, "../public/output/MatchesPlayedPerCity.json"),
      JSON.stringify(matchesPlayedPerCity(matches)),
      (error) => (error ? console.error(error) : null)
    );

    //2.invokes matchesWonPerTeam and obtains the string representation of number of matches won per team
    // writes it to the json file
    fs.writeFile(
      path.join(__dirname, "../public/output/MatchesWonPerTeam.json"),
      JSON.stringify(matchesWonPerTeam(matches)),
      (error) => (error ? console.error(error) : null)
    );

    csv()
      .fromFile(playersPath)
      .then((players) => {
        //3.converts the object of Number of Redcards per Team in a Given Year into JSON that is returned by the getRedcardsPerTeamInYear
        // writes it to the json file
        let year = "2014";
        fs.writeFile(
          path.join(
            __dirname,
            `../public/output/RedcardsIssuedPerTeamIn${year}.json`
          ),
          JSON.stringify(getRedcardsPerTeamInYear(matches, players, year)),
          (error) => (error ? console.error(error) : null)
        );

        //4.converts the object of top given number of players to JSON that is returned by the getTopProbabiltyPlayersToScoreGoal
        let NoOfTopPlayers = 10;
        fs.writeFile(
          path.join(
            __dirname,
            `../public/output/Top${NoOfTopPlayers}ProbabilityPlayersToScoreGoal.json`
          ),
          JSON.stringify(
            getTopProbabiltyPlayersToScoreGoal(players, NoOfTopPlayers)
          ),
          (error) => (error ? console.error(error) : null)
        );
      });
  });
