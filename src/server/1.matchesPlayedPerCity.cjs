function getMatchesPlayedPerCity(matches) {
  let cities = matches.map((match) => {
    return match["City"] != "" ? match["City"].trim() : null;
  });

  let matchesPlayedPerCity = cities.reduce((accu, city) => {
    if (city != null) {
      accu[city] ? accu[city]++ : (accu[city] = 1);
    }
    return accu;
  }, {});

  // console.log(matchesPlayedPerCity)
  return matchesPlayedPerCity;
}

module.exports = getMatchesPlayedPerCity;
